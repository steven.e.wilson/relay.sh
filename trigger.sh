#!/bin/sh -xe

# export TOKEN=... # get this from the web app

curl -X POST -H "Authorization: Bearer $TOKEN" \
   -d '{"data": {"message": "This is a push event"}}' \
   https://api.relay.sh/api/events
