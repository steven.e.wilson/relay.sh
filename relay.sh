#!/bin/sh -xe

# these steps from https://relay.sh/docs/getting-started/

# install relay via brew
# brew install puppetlabs/puppet/relay

# Configure auth (browser window)
# relay auth login

# List existing workflows
relay workflow list

# Is there an command to create the initial workflow?
# e.g: relay workflow create testme ...
relay workflow download testme > hello-world.yaml

# Replace the current definition with something more complex and run it
# relay workflow replace testme hello-world-pass-data.yml
# .. Command "replace" is deprecated, Use `save` instead
relay workflow save testme hello-world-pass-data.yml

# Run the updated workflow and pass the message value from the command line
relay workflow run testme -p message="Pretzel Logic"

# Create and store secrets to be used within the workflow
relay workflow save testme hello-world-secret.yml
